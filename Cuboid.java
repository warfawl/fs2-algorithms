import java.util.*;

public class Solution {
public static void main(String[] args) {
  Map<Integer, Integer> sides = new HashMap<> (); 
  Scanner in = new Scanner(System.in);
  Queue<Integer> maxQueue = new PriorityQueue<>((n1,n2)->n2-n1);
  int N = in.nextInt();
  
  for (int i=0; i<N; i++){
    // int count=0;
    int side = in.nextInt();
    Integer count = sides.get(side);
    count = count == null ? 1 : count+1;
    sides.put(side, count);
    if(count == 4) {
      maxQueue.add(side);
      sides.remove(side);
    }
  }
  

  
  int result =   maxQueue.size()<3 ?-1 :  maxQueue.poll()*maxQueue.poll()*maxQueue.poll() ;
  
  System.out.println(result);  



}

}
